package com.yth.mastermoviles.notificaciones2;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText input;
    TextView tareas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText)findViewById(R.id.textInput);
        tareas = (TextView) findViewById(R.id.tareas);
        Button boton = (Button)findViewById(R.id.boton);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (!input.getText().toString().equals("")) {
                    tareas.append("\n- "+input.getText().toString());
                    input.setText("");
                    Snackbar.make(arg0, "Tarea añadida", Snackbar.LENGTH_LONG)
                            .setAction("Deshacer", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    int lastNewLineAt = tareas.getText().toString().lastIndexOf("\n");
                                    tareas.setText(tareas.getText().toString().substring(0, tareas.getText().toString().lastIndexOf("\n", lastNewLineAt)));
                                }
                            })
                            .show();
                }
            }
        });
    }
}
